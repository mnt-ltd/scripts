# 给Linux创建Swap

如果您的服务器配置比较低，特别是服务器内存，则建议设置下虚拟内存。

参考：https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-20-04

`swap.sh` 是参考上述教程编写的脚本，可根据实际需求修改脚本中的配置以设置虚拟内存大小。