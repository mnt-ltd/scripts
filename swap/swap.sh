#!/bin/sh

echo '创建交换文件'
sudo fallocate -l 4G /swap
ls -lh /swap

echo '设置交换文件权限'
sudo chmod 600 /swap
ls -lh /swap

echo '格式化交换文件'
sudo mkswap /swap

echo '启用交换文件'
sudo swapon /swap
sudo swapon --show
free -h

echo '设置开机自启动'
sudo cp /etc/fstab /etc/fstab.bak
echo '/swap none swap sw 0 0' | sudo tee -a /etc/fstab

echo '设置交换文件调度策略'
echo 'vm.swappiness=10' | sudo tee -a /etc/sysctl.conf

echo '设置交换文件优先级'
echo 'vm.vfs_cache_pressure=50' | sudo tee -a /etc/sysctl.conf
