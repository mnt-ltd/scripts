#!/bin/sh

sudo apt update

echo '安装calibre，电子书管理软件'
# sudo apt install -y calibre
sudo apt install -y libxcb-cursor0 libopengl0 libglx0
echo '如果安装缓慢，请查看教程：https://www.bookstack.cn/read/moredoc/install-calibre.md'
sudo -v && wget --no-check-certificate -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin
echo 'calibre版本：'
calibre --version
sleep 2 # 等待2秒

echo '安装 imagemagick，图片处理软件'
sudo apt install -y imagemagick imagemagick-6.q16
echo 'imagemagick版本：'
convert --version
sleep 2

echo '安装libreoffice，用于处理office文档'
sudo apt install -y libreoffice libreoffice-common
echo 'libreoffice版本：'
soffice --version
sleep 2

echo '安装相应字体以及中文支持'
sudo apt update \
  && sudo apt install -y language-pack-zh-hans \
  && sudo apt install -y language-pack-ja \
  && sudo apt install -y chinese* \
  && sudo apt install -y libreoffice-l10n-zh-cn libreoffice-help-zh-cn \
  && sudo apt install -y libreoffice-help-ja \
  && sudo apt install -y ttf-wqy-zenhei fonts-wqy-microhei
sleep 2

echo '安装mupdf，用于处理pdf文档'
sudo apt install -y mupdf mupdf-tools
sleep 2

echo '安装supervisor，进程管理工具'
sudo apt install -y supervisor
sleep 2

echo '安装inkscape，矢量图形编辑软件，对pdf做兼容处理。可能安装时间比较长，请耐心等待'
sleep 2
sudo apt install -y software-properties-common
sudo add-apt-repository ppa:inkscape.dev/stable
sudo apt update
sudo apt install -y inkscape
sleep 2

echo '测试生成电子书，以验证对中文字体的支持'
echo '您好，世界。hello world！' > hello.txt
ebook-convert hello.txt hello.pdf

echo '生成pdf完成，请查看hello.pdf文件是否正常显示中文'
sleep 5